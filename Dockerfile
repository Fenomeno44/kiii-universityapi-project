FROM openjdk:17
ADD target/kiii-195018-docker.jar kiii-195018-docker.jar
ENTRYPOINT ["java", "-jar","kiii-195018-docker.jar"]
EXPOSE 8080