package mk.ukim.finki.wp.lab.service;

import mk.ukim.finki.wp.lab.exceptions.AlreadyAddedCourse;
import mk.ukim.finki.wp.lab.exceptions.NoStudentPresentInCourse;
import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface CourseService{
    List<Student> listStudentsByCourse(Long courseId);
    Course addStudentInCourse(String username, Long courseId);
    List<Course> listALl();
    Course findById(Long courseId);
    Course save(String name, String description, Long teacherId);
    void delete(Long courseId);
}
