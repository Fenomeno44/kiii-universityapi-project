package mk.ukim.finki.wp.lab.web.controller;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.Teacher;
import mk.ukim.finki.wp.lab.service.CourseService;
import mk.ukim.finki.wp.lab.service.TeacherService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/courses")
public class CourseController {

    private final CourseService courseService;
    private final TeacherService teacherService;

    public CourseController(CourseService courseService, TeacherService teacherService) {
        this.courseService = courseService;
        this.teacherService = teacherService;
    }

    @GetMapping
    public String getCoursesPage(@RequestParam(required = false) String error, Model model) {
        if(error != null && !error.isEmpty()) {
            model.addAttribute("hasError", true);
            model.addAttribute("error", error);
        }
        List<Course> courses = courseService.listALl();
        model.addAttribute("courses", courses);
        model.addAttribute("bodyContent", "courses");
        return "listCourses";
    }

    @PostMapping("/open-course/{id}")
    public String openCourse(@PathVariable Long id, @RequestParam(required = false) String error, Model model){
        List<Student> students = courseService.listStudentsByCourse(id);
        model.addAttribute("students", students);
        return "redirect:/addStudent/{id}";
    }

    @GetMapping("/add-form")
    public String getAddCoursePage(Model model){
        List<Teacher> teachers = this.teacherService.findAll();
        model.addAttribute("teachers",teachers);
        return "add-course";
    }
    @PostMapping("/add")
    public String saveCourse(@RequestParam String name, @RequestParam String description, @RequestParam Long teacher) {
        courseService.save(name, description, teacher);
        return "redirect:/courses";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteCourse(@PathVariable Long id) {
        courseService.delete(id);
        return "redirect:/courses";
    }

    @GetMapping("/edit-form/{id}")
    public String getEditCoursesPage(@PathVariable Long id,Model model){
        if(this.courseService.findById(id) != null){
            Course course = courseService.findById(id);
            List<Teacher> teachers = this.teacherService.findAll();
            model.addAttribute("teachers",teachers);
            model.addAttribute("course",course);
            return "add-course";
        }
        return "redirect:/course?error=CourseNotFound";
    }
}
