package mk.ukim.finki.wp.lab.exceptions;

public class NoStudentPresentInCourse extends Throwable {
    public NoStudentPresentInCourse(String s) {
        System.out.println(s);
    }
}
