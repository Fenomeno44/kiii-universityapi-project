package mk.ukim.finki.wp.lab.service.impl;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Grade;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.repository.jpa.CourseRepositoryJPA;
import mk.ukim.finki.wp.lab.repository.jpa.GradeRepositoryJPA;
import mk.ukim.finki.wp.lab.repository.jpa.StudentRepositoryJPA;
import mk.ukim.finki.wp.lab.service.GradeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GradeServiceImplement implements GradeService {
    private final GradeRepositoryJPA gradeRepository;
    private final StudentRepositoryJPA studentRepository;
    private final CourseRepositoryJPA courseRepository;

    public GradeServiceImplement(GradeRepositoryJPA gradeRepository,
                            StudentRepositoryJPA studentRepository,
                            CourseRepositoryJPA courseRepository) {
        this.gradeRepository = gradeRepository;
        this.studentRepository = studentRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public Character getGradeByStudentId(Long studentId, Long courseId) {
        Student student = studentRepository.findById(studentId).get();
        Course course = courseRepository.findById(courseId).get();
        return gradeRepository.findGradeByStudentAndCourse(student, course);
    }

    @Override
    public List<Grade> findAllByCourse(Long courseId) {
        Course course = courseRepository.findById(courseId).get();
        return gradeRepository.findAllByCourse(course);
    }
}
